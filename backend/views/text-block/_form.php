<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TextBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-block-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'systemName')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::a('<i class="glyphicon glyphicon-arrow-left"></i>' . ' ' . Yii::t('app', 'Back'),
                    Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>

                <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-plus"></i>' . ' ' . Yii::t('app', 'Create') :
                    '<i class="glyphicon glyphicon-pencil"></i>' . ' ' . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
