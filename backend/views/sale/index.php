<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sales');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sale-index">

    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'startDate',
            'finishDate',
            'count',
            'oldPrice',
            'newPrice',
            [
                'label' => Yii::t('app', 'Status'),
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->status) {
                        return Html::a($data->statusLabel , null, ['class' => 'label label-success']);
                    } else {
                        return Html::a($data->statusLabel,
                            null, ['class' => 'label label-danger']);
                    }
                },
                'contentOptions' => [
                    'style' => [
                        'font-size' => '18px',
                    ]
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'options' => [
                    'width' => '3%',
                ],
            ],
        ],
    ]); ?>

</div>
