<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Sale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sale-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'status')->dropDownList($model->getStatusLabels()) ?>

            <?= $form->field($model, 'startDate')->widget(\kartik\datetime\DateTimePicker::className(),
                [
                    'options' => ['placeholder' => 'Select issue date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-m-d H:i:ss',
                        'todayHighlight' => true
                    ]
                ]
            ) ?>

            <?= $form->field($model, 'finishDate')->widget(\kartik\datetime\DateTimePicker::className(),
                [
                    'options' => ['placeholder' => 'Select issue date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-m-d H:i:ss',
                        'todayHighlight' => true
                    ]
                ]
            ) ?>

            <?= $form->field($model, 'count')->textInput() ?>

            <?= $form->field($model, 'oldPrice')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'newPrice')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::a('<i class="glyphicon glyphicon-arrow-left"></i>' . ' ' . Yii::t('app', 'Back'),
                    Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>

                <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-plus"></i>' . ' ' . Yii::t('app', 'Create') :
                    '<i class="glyphicon glyphicon-pencil"></i>' . ' ' . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
