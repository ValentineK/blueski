<?php

return [
    'Start Date' => 'Дата начала',
    'Finish Date' => 'Дата окончания',
    'Count' => 'Колличество',
    'Old Price' => 'Старая цена',
    'New Price' => 'Новая цена',
    'Description' => 'Описание',
    'Created At' => 'Создан',
    'Updated At' => 'Обновлен',
    'Name' => 'Имя',
    'System Name' => 'Системное имя',
    'Text' => 'Текст',
    'Text Blocks' => 'Текстовые блоки',
    'Sales' => 'Акции',
    'Update Text Block' => 'Изменить текстовый блок',
    'Update' => 'Изменить',
    'Update Sale' => 'Изменить акцию',
    'statusLabel' => 'Статус',
    'Active' => 'Активна',
    'Inactive' => 'Неактивна',
    'Status' => 'Статус',

    ];