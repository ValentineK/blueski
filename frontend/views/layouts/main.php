<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png?v=A00wxQzoQo">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png?v=A00wxQzoQo">
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png?v=A00wxQzoQo" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png?v=A00wxQzoQo" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png?v=A00wxQzoQo" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png?v=A00wxQzoQo" sizes="16x16">
    <link rel="manifest" href="/favicon/manifest.json?v=A00wxQzoQo">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg?v=A00wxQzoQo" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico?v=A00wxQzoQo">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png?v=A00wxQzoQo">
    <meta name="theme-color" content="#ffffff">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter27186479 = new Ya.Metrika({ id:27186479, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/27186479" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->

<?php $this->beginBody() ?>


<?= $content ?>



<?php

$js = <<<JS
    var spvParam = 3;
    if ($(window).width() < 768) {
        spvParam = 1;
    }

    var swiper = new Swiper('.rewards .swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: spvParam,
        freeMode: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });

    $(window).resize(function(){
        if ($(window).width() < 768) {
            spvParam = 1;
        } else {
            spvParam = 3;
        }
        swiper.params.slidesPerView = spvParam;
        swiper.updateSlidesSize();
        console.log(swiper.params.slidesPerView);
    });
    //$('#actionModal').modal('show');

JS;
$this->registerJs($js);
?>

<?php
$js = <<<JS
var newYear = new Date();
newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
$('#countdown01').countdown({
            until: newYear
        });
JS;

$this->registerJs($js);
?>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
