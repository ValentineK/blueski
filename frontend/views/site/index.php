<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */
/* @var $modelForModal \frontend\models\ContactFormForModal */

use common\models\Sale;
use common\models\TextBlock;
use yii\bootstrap\ActiveForm;

$this->title = "Гель-лаки BlueSky оптом"
?>

    <div id="wrapper">

        <?php if (Sale::getSale()): ?>
            <div class="action_wrapper">
                <div class="action_button">
                    <a href="" data-target="#actionModal" data-toggle="modal">АКЦИЯ!</a>
                </div>

            </div>
        <?php endif ?>

        <div id="header">
            <div class="header_content">
                <a href="" class="logo">BlueSky</a>

                <div class="city">Красноярск</div>

                <div class="phones">
                    <div>+7 (983) 296-91-89 <span>Красноярск</span></div>
                    <div>+7 (913) 598-05-55 <span>Красноярск</span></div>
                </div>

            </div>
        </div>
        <div id="container">

            <div class="section section01">
                <div class="section_container">
                    <div class="section_content">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="row">

                                    <div class="section_title col-lg-9 col-md-12">
                                        <?= TextBlock::getByShortcut('sales') ?>
                                    </div>

                                </div>
                                <div class="section_description">
                                    <?= TextBlock::getByShortcut('sale_gel_polish') ?>

                                </div>
                            </div>

                            <a href="http://militon.ru" style="position: absolute; left: 100px; ">
                                <?= \yii\helpers\Html::img('/images/militon.png', ['style' => 'height: 350px']) ?>
                            </a>

                            <?php $form = ActiveForm::begin([
                                'options' => [
                                    'class' => 'contact_form',
                                ]
                            ]); ?>

                            <div class="col-md-4 col-sm-12">
                                <div class="reg_form_vert">
                                    <div class="reg_form_inner">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'name')->label('')->textInput(['placeholder' => 'Имя', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Имя"/>-->
                                        </div>


                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'phone')->label('')->textInput(['placeholder' => 'Телефон', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Телефон"/>-->
                                        </div>
                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'email')->label('')->textInput(['placeholder' => 'Электронная почта', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Электронная почта"/>-->
                                        </div>
                                        <div class="buttons">
                                            <button class="button">свяжитесь со мной</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>


                        </div>
                    </div>
                </div>
            </div>
            <div class="section section02">
                <div class="section_container">
                    <div class="section_content">
                        <div class="tabs_wrapper">
                            <ul class="tabs_nav">
                                <li class="active"><a href="#tab01"><?= TextBlock::getByShortcut('gel') ?></a></li>
                                <li><a href="#tab02">оборудование</a></li>
                                <li class="line"></li>
                            </ul>

                            <div class="tab" id="tab01" style="display: block;">
                                <div class="description">
                                    <?= \yii\helpers\Html::img('/images/red-lips.jpg', ['style' => [
                                        'width' => '370px',
                                        'height' => '230px'
                                    ]]) ?>

                                    <div class="desc">
                                        <?= TextBlock::getByShortcut('gel_polish') ?>
                                    </div>
                                </div>
                                <div class="options_wrapper">
                                    <div class="title">Палитра цветов</div>
                                    <ul class="options_nav">
                                        <li class="active"><a href="#op11">Серия shellac</a></li>
                                        <li><a href="#op12">Серия A</a></li>
                                        <li><a href="#op13">Серия S</a></li>
                                        <li><a href="#op14">Серия KG / <span class="color: red">Акция</span></a></li>
                                        <!--                                        <li><a href="#op14">Оборудование</a></li>-->
                                    </ul>

                                    <!--                                    <div class="option_content" id="op10" style="display: block;">-->
                                    <!--                                        --><? //= \yii\helpers\Html::img('/images/slider/tab1/red-lips.jpg')?>
                                    <!--                                    </div>-->
                                    <div class="option_content" id="op11" style="display: block;">
                                        <?= \yii\helpers\Html::img('/images/slider/tab1/seriesShellac.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op12">
                                        <?= \yii\helpers\Html::img('/images/slider/tab1/seriesA.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op13">
                                        <?= \yii\helpers\Html::img('/images/slider/tab1/seriesS.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op14">
                                        <?= \yii\helpers\Html::img('/images/slider/tab1/seriesKG.jpg', ['style' => 'max-width: 730px']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab" id="tab02">
                                <div class="description">
                                    <?= \yii\helpers\Html::img('/images/equipment.jpg') ?>

                                    <div class="desc">
                                        <?= TextBlock::getByShortcut('equipment') ?>
                                    </div>
                                </div>
                                <div class="options_wrapper">
                                    <div class="title">Оборудование</div>
                                    <ul class="options_nav">
                                        <li class="active"><a href="#op21">Аппараты для маникюра</a></li>
                                        <li><a href="#op22">Уф лампы 36W</a></li>
                                        <li><a href="#op23">LED+CCFL 36W</a></li>
                                        <li><a href="#op24">LED+CCFL 48W</a></li>
                                    </ul>

                                    <div class="option_content" id="op21" style="display: block">
                                        <?= \yii\helpers\Html::img('/images/slider/tab2/manicure.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op22">
                                        <?= \yii\helpers\Html::img('/images/slider/tab2/uf-36.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op23">
                                        <?= \yii\helpers\Html::img('/images/slider/tab2/led-36.jpg') ?>
                                    </div>
                                    <div class="option_content" id="op24">
                                        <?= \yii\helpers\Html::img('/images/slider/tab2/led-48.jpg') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section03">
                <div class="section_container">
                    <div class="section_content">
                        <div class="section_title">КАК МЫ РАБОТАЕМ?</div>

                        <ul class="how">
                            <li>Вы связываетесь с нами через <a href="">форму</a></li>
                            <li>Мы высылаем прайс и информацию о заказе</li>
                            <li>Вы производите оплату и выбираете способ доставки</li>
                            <li>Мы высылаем ваш заказ сразу после его укомплектования</li>
                        </ul>

                    </div>
                </div>
            </div>


            <div class="section section_form">
                <div class="section_container">
                    <div class="section_content">


                        <?php $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'contact_form',
                            ]
                        ]); ?>


                        <div class="reg_form_hor">
                            <div class="row">
                                <div class="col-md-9 col-sm-12">
                                    <div class="title">Свяжитесь с нами для оформления заказа!</div>

                                    <div class="row">
                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'name')->label('')->textInput(['placeholder' => 'Имя', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Имя"/>-->
                                        </div>


                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'phone')->label('')->textInput(['placeholder' => 'Телефон', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Телефон"/>-->
                                        </div>
                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'email')->label('')->textInput(['placeholder' => 'Электронная почта', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control"
                                                   placeholder="Электронная почта"/>-->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 buttons">
                                    <button class="button">свяжитесь со мной</button>
                                </div>
                            </div>
                        </div>


                        <?php ActiveForm::end(); ?>


                    </div>
                </div>
            </div>

            <div class="section section04">
                <div class="section_container">
                    <div class="section_content">
                        <div class="row">
                            <div class="section_title col-md-6 col-sm-12"><?= TextBlock::getByShortcut('send') ?></div>
                        </div>
                        <div class="row">
                            <div class="section_description col-md-7 col-sm-12">
                                <?= TextBlock::getByShortcut('send_tomorow') ?>

                            </div>
                        </div>

                        <div class="row delivery">
                            <div class="col-md-6 col-sm-12">
                                <div class="title">Мы работаем с компаниями:</div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/partner1.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/partner2.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/partner3.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/partner4.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="title">Принимаем к оплате:</div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/pay_wm.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/pay_qiwi.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/pay_card.png" alt=""/>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <img src="../images/pay_cash.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section05">
                <div class="section_container">
                    <div class="section_content">
                        <div class="row">
                            <div class="section_title col-md-5 col-sm-12">Почему выгодно работать с нами?</div>
                        </div>
                        <ul class="why">
                            <li>Самая выгодная цена</li>
                            <li>Вся продукция в наличии</li>
                            <li>Огромный выбор цветов</li>
                            <li>Продукция сертифицирована</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="section section_form">
                <div class="section_container">
                    <div class="section_content">
                        <?php $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'contact_form',
                            ]
                        ]); ?>


                        <div class="reg_form_hor">
                            <div class="row">
                                <div class="col-md-9 col-sm-12">
                                    <div class="title">Свяжитесь с нами для оформления заказа!</div>

                                    <div class="row">
                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'name')->label('')->textInput(['placeholder' => 'Имя', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Имя"/>-->
                                        </div>

                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'phone')->label('')->textInput(['placeholder' => 'Телефон', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control" placeholder="Телефон"/>-->
                                        </div>
                                        <div class="form-group col-sm-4 col-xs-12">
                                            <label for="" class="control-label"></label>
                                            <?= $form->field($model, 'email')->label('')->textInput(['placeholder' => 'Электронная почта', 'class' => 'form-control']) ?>
                                            <!--<input type="text" name="" id="" class="form-control"
                                                   placeholder="Электронная почта"/>-->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 buttons">
                                    <button class="button">свяжитесь со мной</button>
                                </div>
                            </div>
                        </div>


                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>


            <div class="section section_testimonials">
                <div class="section_container">
                    <div class="section_content">

                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="section_title">Сертификаты</div>

                                <div class="rewards">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <a href="/images/s1.jpg" class="colorbox"><img src="/images/s1.jpg"
                                                                                               alt="" height="220"/></a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a href="/images/s2.jpg" class="colorbox"><img src="/images/s2.jpg"
                                                                                               alt="" height="220"/></a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a href="/images/s3.jpg" class="colorbox"><img src="/images/s3.jpg"
                                                                                               alt="" height="220"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-12">

                                <div class="testimonial">
                                    <div class="section_title">КЛИЕНТЫ О НАС</div>
                                    <img src="/images/face.jpg" alt=""/>

                                    <div class="description">
                                        Давно работаю, всё нравится, особенно внезапные подарочки :)
                                    </div>
                                    <div class="name">
                                        Наталья Фогельзанг, <span>Томск</span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="section section06">
                <div class="section_container">
                    <div class="section_content">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="section_title">У ВАС ОСТАЛИСЬ ВОПРОСЫ?</div>

                                <div class="section_description">
                                    Просто заполните форму и мы ответим на все интересующие вас вопросы!
                                </div>

                                <div class="contact_phones">
                                    <div>+7 (983) 296-91-89 <span>Красноярск</span></div>
                                    <div>+7 (913) 598-05-55 <span>Красноярск</span></div>
                                </div>

                                <div class="contact_address">
                                    <div>г.Красноярск</div>
                                    <div>ул.Влетная д.5, стр.2, оф.114</div>
                                </div>
                            </div>

                            <a href="http://militon.ru" style="position: absolute; left: 50%; ">
                                <?= \yii\helpers\Html::img('/images/militon.png', ['style' => 'height: 250px']) ?>
                            </a>

                            <?php $form = ActiveForm::begin([
                                'options' => [
                                    'class' => 'contact_form',
                                ]

                            ]); ?>

                            <div class="col-md-4 col-sm-12">
                                <div class="reg_form_vert">
                                    <div class="reg_form_inner">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'name')->label('')->textInput(['placeholder' => 'Имя', 'class' => 'form-control']) ?>
                                        </div>

                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'phone')->label('')->textInput(['placeholder' => 'Телефон', 'class' => 'form-control']) ?>
                                        </div>
                                        <div class="form-group">
                                            <label for=""></label>
                                            <?= $form->field($model, 'email')->label('')->textInput(['placeholder' => 'Электронная почта', 'class' => 'form-control']) ?>
                                        </div>
                                        <div class="buttons">
                                            <button class="button">свяжитесь со мной</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="footer">
        <div class="footer_content">
            <a href="" class="logo">BlueSky</a>

            <div class="city">Красноярск</div>

            <div class="phones">
                <div>+7 (983) 296-91-89 <span>Красноярск</span></div>
                <div>+7 (913) 598-05-55 <span>Красноярск</span></div>
            </div>

        </div>
    </div>

<?= \frontend\widgets\SaleWidget::widget([
    'model' => $saleForm,
]) ?>

    <!-- /.modal -->
    <div class="modal fade action-modal" id="actionModalSucess">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <div class="message_success">
                        <div class="title">Ваша заявка успешно принята!</div>

                        <div class="description">
                            Спасибо за ваше обращение, сейчас все менеджеры отдыхают, но они с радостью перезвонят вам:
                            <ul>
                                <li>- Понедельник - пятница с 9:00 - 18:00</li>
                                <li>- Суббота с 10:00 - 17:00</li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php $js = <<<'JS'
$('.colorbox').colorbox({
    width: '80%',
});
$(".contact_form").on("beforeSubmit", function(ev) {
    var $form = $(ev.target);
    $.ajax({
        url: $form.attr('action'),
        type: "POST",
        dataType: "json",
        data: $form.serialize(),
        success: function(response) {
            $('#actionModalSucess').modal('show');
            $('#actionModal').modal('hide');
            setTimeout(function() {
                $('#actionModalSucess').modal('hide');
            }, 3000);
            $(".contact_form").find("input, button").prop("disabled", true);
        },
        error: function(response) {}
    });

    return false;
})
JS;
$this->registerJs($js);
?>