<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $css = [
        '/themes/main/css/reset.css',
        '/themes/main/css/swiper.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        '/themes/main/css/style.css',
        '/css/colorbox.css',
    ];
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $js = [
        '/themes/main/js/swiper.jquery.min.js',
        '/themes/main/js/jquery.plugin.min.js',
        '/themes/main/js/jquery.countdown.min.js',
        '/themes/main/js/jquery.countdown-ru.js',
        '/themes/main/js/scripts.js',

    ];

    public $depends = [
        'frontend\assets\ColorboxAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
