<?php

namespace frontend\models;

use common\models\Sale;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * ContactFormForModal is the model behind the contact form.
 * @var string $name
 * @var string $email
 * @var string $phone
 * @var string $message
 * @var integer $count
 * @var integer $maxCount
 */
class ContactFormForModal extends Model
{
    public $name;
    public $phone;
    public $email;
    public $count;

    /** @var Sale|null */
    public $sale;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone', 'count'], 'required', 'message' => '*Обязательное поле'],
            ['email', 'email', 'message' => 'Некорректный E-mail'],
            ['count', 'integer', 'max' => ArrayHelper::getValue($this->sale, 'count', 0), 'tooBig' => 'Не больше ' . ArrayHelper::getValue($this->sale, 'count', 0)]

        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $html = [
            Html::tag('h1', "Заявка с сайта Blueski.ru(АКЦИЯ)"),
            Html::tag('p', "Имя клиента: {$this->name}"),
            Html::tag('p', "Телефон: {$this->phone}"),
            Html::tag('p', "Email: {$this->email}"),
            Html::tag('p', "Количество: {$this->count}")
        ];

        return Yii::$app->mailer->compose()
            ->setTo(ArrayHelper::getValue(Yii::$app->params, 'adminEmail'))
            ->setSubject("Заявка с сайта Blueski.ru(АКЦИЯ)")
            ->setHtmlBody(implode('', $html))
            ->send();
    }
}
