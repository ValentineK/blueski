<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 * @var string $name
 * @var string $email
 * @var string $phone
 * @var string $message
 * @var integer $count
 * @var integer $maxCount
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone'], 'required', 'message' => '*Обязательное поле'],
            ['email', 'email', 'message' => 'Некорректный E-mail'],

        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $html = [
            Html::tag('h1', "Заявка с сайта Blueski.ru"),
            Html::tag('p', "Имя клиента: {$this->name}"),
            Html::tag('p', "Телефон: {$this->phone}"),
            Html::tag('p', "Email: {$this->email}"),
//            Html::tag('p', "Количество: {$this->count}")
        ];

        return Yii::$app->mailer->compose()
            ->setTo(ArrayHelper::getValue(Yii::$app->params, 'adminEmail'))
            ->setSubject("Заявка с сайта Blueski.ru")
            ->setHtmlBody(implode('', $html))
            ->send();
    }
}
