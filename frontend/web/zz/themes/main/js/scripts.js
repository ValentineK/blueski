$(document).ready(function() {

    $('.tabs_nav li a').click(function(){

        var el = $(this),
            parent = el.parent('li'),
            nav = el.parents('ul'),
            cont = el.parents('.tabs_wrapper'),
            target = $(el.attr('href')),
            line = nav.find('.line');

        nav.find('.active').removeClass('active');
        parent.addClass('active');

        var lPos = nav.find('.active').position().left + 6;

        line.css('left', lPos+'px');

        cont.find('.tab').hide();
        target.show();

        return false;
    });
    $('.options_nav li a').click(function(){

        var el = $(this),
            parent = el.parent('li'),
            nav = el.parents('ul'),
            cont = el.parents('.options_wrapper'),
            target = $(el.attr('href'));

        nav.find('.active').removeClass('active');
        parent.addClass('active');
        cont.find('.option_content').hide();
        target.show();

        return false;
    });

});

$(window).load(function() {
});
$(window).resize(function() {
});
$(window).scroll( function(){
});

