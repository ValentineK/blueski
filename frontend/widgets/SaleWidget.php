<?php

namespace frontend\widgets;

use common\models\Sale;
use yii\base\Widget;
use yii\helpers\VarDumper;

/**
 * @package app\widgets
 */
class SaleWidget extends Widget
{
    public $model;
    public $form;

    /**
     * @var string
     */

    public function init()
    {
        parent::init();
    }

    /**
     * @return null|string
     */
    public function run()
    {
        $sale = Sale::getSale();

        if(null === $sale) {
            return;
        }

        return $this->render('sale-widget', [
            'sale' => $sale,
            'model' => $this->model,
            'form' => $this->form,
        ]);
    }
}