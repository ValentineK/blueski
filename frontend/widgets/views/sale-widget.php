<?php
use yii\bootstrap\ActiveForm;

/**
 * @var \common\models\Sale $sale
 */
?>

<?php
$date = DateTime::createFromFormat('Y-m-d H:i:s', $sale->finishDate);
$msTS = $date->getTimestamp() * 1000;
$js = <<<JS2
$('#countdown01').countdown({
    until: new Date({$msTS})
});
JS2;

$this->registerJs($js);
?>

<div class="modal fade action-modal" id="actionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">

                <div class="action_description">
                    <div class="countdown_wrapper">
                        <div class="title">до конца акции:</div>
                        <div id="countdown01"></div>
                    </div>

                    <div class="action_options row">
                        <div class="col-sm-4">
                            <div class="option left">
                                <span>осталось лаков:</span>

                                <div><?= $sale->count ?></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="option old_price">
                                <span>по супер-цене</span>

                                <div><?= Yii::$app->formatter->asDecimal($sale->oldPrice, 0) . 'р/шт.' ?></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="option new_price">
                                <div><?= Yii::$app->formatter->asDecimal($sale->newPrice, 0) . 'р/шт.' ?></div>
                            </div>
                        </div>
                    </div>


                </div>

                <p>
                    <?= $sale->description == null ?
                        'Это работает как совместная покупка, вы отправляете заявку, менеджер подтверждает её, как только'
                        . ' наберется заявок на ХХХ флаконов, мы закрываем акцию, все участники оплачивают и мы рассылаем'
                        . ' всем по почте ваши заказы.' :
                        $sale->description
                    ?>
                </p>
                <br>


                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'contact_form',
                    ]

                ]); ?>
                <div class="reg_form_hor">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label for="" class="control-label"></label>
                                    <?= $form->field($model, 'name')->label('')->textInput(['placeholder' => 'Имя', 'class' => 'form-control']) ?>
                                </div>
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label for="" class="control-label"></label>
                                    <?= $form->field($model, 'count')->label('')->textInput(['placeholder' => 'Колличество', 'class' => 'form-control']) ?>
                                </div>
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label for="" class="control-label"></label>
                                    <?= $form->field($model, 'phone')->label('')->textInput(['placeholder' => 'Телефон', 'class' => 'form-control']) ?>
                                </div>
                                <div class="form-group col-sm-6 col-xs-12">
                                    <label for="" class="control-label"></label>
                                    <?= $form->field($model, 'email')->label('')->textInput(['placeholder' => 'Почта', 'class' => 'form-control']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 buttons">
                            <div class="buttons">
                                <button class="button">свяжитесь со мной</button>
                            </div>
                        </div>
                    </div>
                </div>


                <?php ActiveForm::end(); ?>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
