<?php

namespace common\models;

use common\components\TimestampBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%text_block}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $systemName
 * @property string $text
 * @property string $createdAt
 * @property string $updatedAt
 */
class TextBlock extends \common\components\ActiveRecord
{

    protected static $results = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%text_block}}';
    }

    /**
     * @param $shortcut
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getByShortcut($shortcut)
    {
        if (empty(static::$results)) {
            self::$results = self::find()->indexBy('systemName')->all();
        }

        return ArrayHelper::getValue(self::$results, $shortcut . '.text');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'systemName'], 'required'],
            [['text'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['name', 'systemName'], 'string', 'max' => 255],
            [['systemName'], 'match', 'pattern' => '/[a-z\d]+/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'systemName' => Yii::t('app', 'System Name'),
            'text' => Yii::t('app', 'Text'),
            'createdAt' => Yii::t('app', 'Created At'),
            'updatedAt' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
