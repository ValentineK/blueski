<?php

namespace common\models;

use Yii;
use common\components\TimestampBehavior;
use common\traits\StatusTrait;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "{{%sale}}".
 *
 * @property integer $id
 * @property string $startDate
 * @property string $finishDate
 * @property integer $count
 * @property string $oldPrice
 * @property string $newPrice
 * @property string $description
 * @property string $status
 * @property string $createdAt
 * @property string $updatedAt
 */
class Sale extends \common\components\ActiveRecord
{
    use StatusTrait;

    protected static $sale = [];

    const STATUS_INACTIVE= 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'oldPrice', 'newPrice'], 'required'],
            [['startDate', 'finishDate', 'status', 'createdAt', 'updatedAt'], 'safe'],
            [['count', 'status'], 'integer'],
            [['oldPrice', 'newPrice'], 'number'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'startDate' => Yii::t('app', 'Start Date'),
            'finishDate' => Yii::t('app', 'Finish Date'),
            'count' => Yii::t('app', 'Count'),
            'oldPrice' => Yii::t('app', 'Old Price'),
            'newPrice' => Yii::t('app', 'New Price'),
            'description' => Yii::t('app', 'Description'),
            'statusLabels' => Yii::t('app', 'Status'),
            'status' => Yii::t('app', 'Status'),
            'createdAt' => Yii::t('app', 'Created At'),
            'updatedAt' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
        ];
    }

    /**
     * @return array|null|Sale
     */
    public static function getSale()
    {
        if(count(self::$sale) == 0) {
            self::$sale = self::find()
                ->andWhere(['>', 'count', 0,])
                ->andWhere('startDate < NOW()')
                ->andWhere('finishDate > NOW()')
                ->andWhere(['status' => 1])
                ->one();
        }

        return self::$sale;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
