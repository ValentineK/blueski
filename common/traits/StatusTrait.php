<?php

namespace common\traits;

use yii\helpers\ArrayHelper;

/**
 * Trait for default status column
 *
 * Class StatusTrait
 * @package common\components
 */
trait StatusTrait
{
    /**
     * @param null $default
     * @return mixed
     */
    public function getStatusLabel($default = null)
    {
        return ArrayHelper::getValue(static::getStatusLabels(), $this->status, $default);
    }
}