<?php

namespace common\components;
use yii\helpers\VarDumper;

/**
 * Class ActiveRecord
 * @package common\components
 */
class ActiveRecord extends \yii\db\ActiveRecord
{

    /**
     * @param $runValidation
     * @param $attributeNames
     * @return bool
     */
    public function trySave($runValidation = true, $attributeNames = null)
    {
        if (false === $this->save($runValidation, $attributeNames)) {
            throw new \LogicException("Error while saving model: " . VarDumper::dumpAsString($this->getErrors()));
        }
        return true;
    }

    /**
     * @param $runValidation
     * @param $attributeNames
     * @return bool
     */
    public function tryUpdate($runValidation = true, $attributeNames = null)
    {
        if (false === $this->update($runValidation, $attributeNames)) {
            throw new \LogicException("Error while updating model: " . VarDumper::dumpAsString($this->getErrors()));
        }
        return true;
    }

    /**
     * @param $runValidation
     * @param $attributeNames
     * @return bool
     */
    public function tryInsert($runValidation = true, $attributeNames = null)
    {
        if (false === $this->insert($runValidation, $attributeNames)) {
            throw new \LogicException("Error while inserting model: " . VarDumper::dumpAsString($this->getErrors()));
        }
        return true;
    }
}