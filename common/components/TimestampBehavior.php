<?php
/**
 * @author Alexey Samoylov <alexey.samoylov@gmail.com>
 */
namespace common\components;

use yii\db\Expression;

class TimestampBehavior extends \yii\behaviors\TimestampBehavior
{
    public $createdAtAttribute = 'createdAt';
    public $updatedAtAttribute = 'updatedAt';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->value = new Expression('NOW()');
        parent::init();
    }
}