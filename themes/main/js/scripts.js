$(document).ready(function() {

    $(window).trigger('scroll');

    $('.tabs_nav li a').click(function(){

        var el = $(this),
            parent = el.parent('li'),
            nav = el.parents('ul'),
            cont = el.parents('.tabs_wrapper'),
            target = $(el.attr('href')),
            line = nav.find('.line');

        nav.find('.active').removeClass('active');
        parent.addClass('active');

        var lPos = nav.find('.active').position().left + 6;

        line.css('left', lPos+'px');

        cont.find('.tab').hide();
        target.show();

        return false;
    });
    $('.options_nav li a').click(function(){

        var el = $(this),
            parent = el.parent('li'),
            nav = el.parents('ul'),
            cont = el.parents('.options_wrapper'),
            target = $(el.attr('href'));

        nav.find('.active').removeClass('active');
        parent.addClass('active');
        cont.find('.option_content').hide();
        target.show();

        return false;
    });

});

$(window).load(function() {
});
$(window).resize(function() {
});
$(window).scroll( function(){


    $('.section_container.animate').each(function (i) {

        var top_of_object = $(this).offset().top;
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        if (bottom_of_window > top_of_object + 200) {

            //$(this).animate({'opacity':'1'},600);
            $(this).addClass('show');

        }
    });


});

