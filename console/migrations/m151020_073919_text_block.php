<?php

use yii\db\Migration;

class m151020_073919_text_block extends Migration
{
    public function up()
    {
        $this->createTable('{{%text_block}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->notNull(),
            'systemName' => $this->string()->notNull(),
            'text' => $this->text(),
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%text_block}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
