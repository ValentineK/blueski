<?php

use yii\db\Migration;

class m151026_050822_root_user_insert extends Migration
{
    public function up()
    {
        $this->insert('{{%user}}', [
            'id' => getenv('APP_DEFAULT_USER_ID'),
            'auth_key' => 'blueski-auth-key',
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash("blueski2015Vova"),
            'email' => 'jelezo1968@gmail.com',
            'status' => 10,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()')
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', ['username' => 'admin']);
    }
}
