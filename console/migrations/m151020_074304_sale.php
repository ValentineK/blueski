<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_074304_sale extends Migration
{
    public function up()
    {
        $this->createTable('{{%sale}}', [
            'id' => $this->primaryKey(),
            'startDate' => $this->dateTime()->notNull(),
            'finishDate' => $this->dateTime()->notNull(),
            'count' => $this->integer()->notNull(),
            'oldPrice' => $this->money(10,4)->notNull(),
            'newPrice' => $this->money(10, 4)->notNull(),
            'description' => $this->text(),
            'status' => $this->smallInteger(),
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%sale}}');
    }
}
