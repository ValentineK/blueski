## Installation ##

```bash
cd docker
cp ../.env.example ../.env
cp ../docker-compose.override.example.yml ../docker-compose.override.yml
docker volume create --name=gel_blueski_mysql_data --driver=local
docker-compose build
docker-compose up -d nginx
docker-compose run --rm workspace composer install
docker-compose run --rm workspace php init
docker-compose run --rm yii migrate
```

Now app must be working:

* frontend app: [http://localhost:8001](http://localhost:8001)
* backend app: [http://localhost:8002](http://localhost:8002)
* guest app: [http://localhost:8003](http://localhost:8003)
